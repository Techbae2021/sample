# BASE IMAGE
FROM node:12.18.4-alpine
# ADDITIONAL LIBRARIES NEEDED FOR BUILD
RUN apk update; \
    apk add libpng-dev; \
    apk add autoconf; \
    apk add automake; \
    apk add make; \
    apk add g++; \
    apk add libtool; \
    apk add nasm; \
    apk add nginx; \
    mkdir /run/nginx/; \
    mkdir /usr/share/nginx; \
    mkdir /usr/share/nginx/html;
WORKDIR /usr/share/nginx/html
COPY . /usr/share/nginx/html
# OUR CONFIGURATION FILES FOR ENVIRONMENT VARIABLES
COPY $PWD/docker/entrypoint.sh /usr/local/bin
# NGINX CONFIGURATION TO MAKE OUR PUBLIC FOLDER ACCESSIBLE
COPY $PWD/docker/default.conf /etc/nginx/conf.d/default.conf
RUN chmod +x /usr/local/bin/entrypoint.sh
ENTRYPOINT ["/bin/sh", "/usr/local/bin/entrypoint.sh"]
RUN npm install --non-interactive --frozen-lockfile
CMD ["/bin/sh", "-c", "exec nginx -g 'daemon off;';"]

# Stage 1: Build Gatsby
# FROM node:alpine AS build

# RUN apk update; \
#     apk add libpng-dev; \
#     apk add autoconf; \
#     apk add automake; \
#     apk add make; \
#     apk add g++; \
#     apk add libtool; \
#     apk add nasm; \
#     apk add nginx; \
#     mkdir /run/nginx/; \
#     mkdir /usr/share/nginx; \
#     mkdir /usr/share/nginx/html;

# RUN npm install -g gatsby-cli

# WORKDIR /app
# COPY ./package.json .
# RUN npm install && npm cache clean
# # RUN yarn cache clean && yarn install
# COPY . .
# CMD ["npm", "build", "-H", "0.0.0.0" ]

# # Stage 2: Serve the site
# FROM nginx:mainline-alpine
# RUN rm /usr/share/nginx/html/*
# COPY --from=build /app/public /usr/share/nginx/html/
# RUN ls usr/share/nginx/html -l